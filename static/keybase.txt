==================================================================
https://keybase.io/drizzt
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://redaelli.eu
  * I am drizzt (https://keybase.io/drizzt) on keybase.
  * I have a public key ASDfA-Ofy8MC_bXRxuqNfdKUbuzv8oh0RpUS8GDP5tO5vQo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0101d3ae15fd0a47c6a360ab07952aebcf214ef5a4ed7452614446d307f363b2f90a0a",
      "host": "keybase.io",
      "kid": "0120df03e39fcbc302fdb5d1c6ea8d7dd2946eeceff28874469512f060cfe6d3b9bd0a",
      "uid": "8b65997ebbe68f689da6633620ae8700",
      "username": "drizzt"
    },
    "merkle_root": {
      "ctime": 1547029469,
      "hash": "f0684bd4c6b39b8142d7b28133119f2294b3ac4c22c500a00042a3b44af2936fddf34a7c293f6f3fee4b24d545d1cf2a5b1c2858ceb21d325b1252c5f3994ce4",
      "hash_meta": "aedb17056e507493f0974e505e7f9dad87208510f949f113dbc3fdecb1f91244",
      "seqno": 4257939
    },
    "service": {
      "entropy": "L0DaYWCe9CSVYiSHO/DaeNQX",
      "hostname": "redaelli.eu",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "2.13.0"
  },
  "ctime": 1547029475,
  "expire_in": 504576000,
  "prev": "912a7a6161abdcf0a0c6abb74250e4265b17193f9412e70d2d49885f5107d022",
  "seqno": 61,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg3wPjn8vDAv210cbqjX3SlG7s7/KIdEaVEvBgz+bTub0Kp3BheWxvYWTESpcCPcQgkSp6YWGr3PCgxqu3QlDkJlsXGT+UEucNLUmIX1EH0CLEIFayA+VK3M3O1XmkJcELouhB0XCEJtAFSPLOAJOb5zJvAgHCo3NpZ8RAACndCd5xYMydcV1nW+wJfRy1KVINOyWVVKMRsI/GDpLbNPJh6iMD+qFABR3r1qzoS201YKKwqhJybJR3ndKYAKhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEINmc9Qq+322sYioZUh3saRWY8mJylmcvWhHnrv96pPBro3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/drizzt

==================================================================
