+++
title = "Timothy Mario Redaelli"
tagline = "Software Designer"
+++

I'm (also) a software designer and security specialist.
I actively participate in the development of several open source projects.
I firmly believe in the philosophy of free software.

Currently I'm working as Senior Software Engineer at [Red Hat](https://www.redhat.com/) in Milan (Italy).
